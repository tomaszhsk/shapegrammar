# README #
Aplikacja obrazująca działanie algorytmu ewolucyjnego opartego na zbiorach danych wygenerowanych przy użyciu gramatyki kształtu.

### Opis działania ###
Przy użyciu gramatyki kształtu generujemy pięć zbiorów danych. 
Każdy zbiór danych zaczyna od jednego, początkowego kształtu (x: 200, y: 200) znajdującego się na środku svg o rozmiarach(400, 400). 
Następnie, dla każdego zbioru danych algorytm algorytm wykonuje 400 kroków w których 

1. wybiera kształty na których nie została użyta produkcja
2. spośród wyżej wybranych kształtów losuje jeden kształt i używa na nim produkcji

Na koniec algorytm ocenia powstały zbiór danych zadaną funkcją oceny. 
Generowanie zbioru danych i funkcja oceny znajdują się w pliku 
```
#!
/grammar-app/src/controllers/TransformController.js
```

Po wygenerowaniu zbiorów danych użytkownik ma możliwość wykonać jeden demonstracyjny krok algorytmu ewolucyjnego.
W kroku tym sortujemy malejąco zbiory danych po indeksie przyznanym każdemu z nich przez funkcję oceny.
Następnie scalamy zbiory danych 0 i 1 oraz 2 i 3 a czwarty odrzucamy. 

Powstają dwa nowe zbiory danych które są sumą(unią) dwóch zbiorów.


### Jak uruchomić? ###

* Aby uruchomić aplikację w trybie produkcyjnym należy przejść do ścieżki 
```
#!

build
```
 i uruchomić plik Index.html
* Pliki js i css wymagane do działania aplikacji zostały zminifikowane i spakowane do pojedynczych plików znajdujących się pod ścieżką 
```
#!

/build/static
```


* Aplikacja działa przy użyciu Node.js. Node.js jest niezbędny do uruchomienia aplikacji w trybie developerskim
* Aby uruchomić aplikację w trybie developerskim należy w konsoli przejść do ścieżki 
```
#!

/grammar-app
```
 i uruchomić polecenie 'npm start'


----------------------------------------------------------



* To run the app in production mode navigate to 
```
#!

/build
```
 and open Index.html
* All needed javascript and css files are minified, bundled and supplied in 
```
#!

/build/static
```
 directory


* This app runs on Node.js. Node.js is needed to run the app in development mode
* To run the app in development mode navigate to 
```
#!

/grammar-app
```
 in command line and run 'npm start' command