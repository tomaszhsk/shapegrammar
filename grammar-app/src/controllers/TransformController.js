import _ from 'lodash';

export default class TransformController {

    transformDataset = function(dataSet, iterations) {
        this.dataSet = dataSet;

        if(!iterations)
            iterations = 400;

        for(var i=0; i<iterations; i++)
            this.transform(dataSet)

        let shapeRate = this.calcShapeRate();
        return { rate: shapeRate, dataSet: this.dataSet};
    };

    calcShapeRate() {

        var rate = _.reduce(this.dataSet, function(sum, n){
            return sum + n.x + n.y;
        }, 0);

        var divided = rate/100000;

        return divided;
    }

    transform (){
        var withoutChildren = _.filter(this.dataSet, function(e){
            return e.hasChildren === undefined ||
                e.hasChildren === false;
        });

        var selectedItem = withoutChildren[Math.floor(Math.random()*withoutChildren.length)];

        selectedItem.hasChildren = true;

        var isRotated = selectedItem.transform !== undefined && selectedItem.transform !== "";
        this.updateData(selectedItem.x, selectedItem.y, isRotated);
    };

    updateData (xCord, yCord, rotated){
        var locationX, locationY;
        if(rotated){
            locationX = xCord;
            locationY = yCord - window.CONSTANTS.elemWidth;

            this.appendToDataSet(locationX, locationY, false);
            locationY = yCord + window.CONSTANTS.elemWidth;
            this.appendToDataSet(locationX, locationY, false);
        }
        else{
            locationX = xCord - window.CONSTANTS.elemWidth;
            locationY = yCord;

            this.appendToDataSet(locationX, locationY, true);
            locationX = xCord + window.CONSTANTS.elemWidth;
            this.appendToDataSet(locationX, locationY, true);
        }

    };

    appendToDataSet (x, y, shouldTransform){
        var elemAlreadyExists = _.find(this.dataSet, function(e){ return e.x === x && e.y === y;});
        if(elemAlreadyExists !== undefined)
            return;

        var rotateFunc = shouldTransform ? this.calcRotate(x, y) : "";
        this.dataSet.push({
            x: x,
            y: y,
            transform: rotateFunc
        });
    };

    calcRotate(x, y){
        var rotateX = x + window.CONSTANTS.elemWidth/2;
        var rotateY = y + window.CONSTANTS.elemWidth/2;
        return "rotate(45 " + rotateX + " " + rotateY + ")";
    };
}