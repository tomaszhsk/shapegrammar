import React, { Component } from 'react';
import * as d3 from "d3";

class Grammar extends Component {
    constructor(props){
        super(props);

        var id = 'svg' + this.props.index;
        this.state = {
            dataSet: this.props.dataSet,
            id: id,
            rate: this.props.rate
        }
    }

    draw(){
        var idSelector = '#' + this.state.id;
        d3.select(idSelector).selectAll("*").remove();

        var svgContainer = d3.select(idSelector).append('svg')
            .attr('width', 400)
            .attr('height', 400);



        var dataSet = this.state.dataSet;

        var nodes = svgContainer.selectAll("rect").data(dataSet);
        var rectangles = nodes.enter().append("rect");

        rectangles
            .attr("x", function(d){
                return d.x;})
            .attr("y", function(d){return d.y;})
            .attr("transform", function(d){return d.transform;})
            .attr("stroke-width", 2)
            .attr("stroke", "black")
            .attr("fill-opacity", "0")
            .attr("width", window.CONSTANTS.elemWidth)
            .attr("height", window.CONSTANTS.elemWidth)
            .attr("transform", function(d) { return d.transform;});
    }

    componentDidMount(){
        this.draw();
    }

    componentDidUpdate(){
        let id = '#svg' + this.state.id;
        d3.select(id).remove();
        this.draw();
    }

    componentWillReceiveProps(nextProps){
        this.setState((prevState, props) => {
            return {
                dataSet: nextProps.dataSet,
                id: 'svg' + nextProps.index,
                rate: nextProps.rate
            };
        });
    }

    render(){
        return(
            <div>
                <div className="row h2">
                    <div>Shape rate : {this.props.rate}</div>
                </div>
                <div className="row" id={this.state.id}></div>
            </div>
        );
    }
}
export default Grammar;