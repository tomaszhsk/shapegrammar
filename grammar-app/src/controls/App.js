import React, { Component } from 'react';
import './../App.css';
import _ from 'lodash';
import Grammar from './Grammar';
import TransformController from './../controllers/TransformController';


class App extends Component {

    constructor(props){
        super(props);
        this.setInitialData();

        let sets = this.getInitialDataSets();

        this.state = {
            iteration: 0,
            sets: 5,
            dataSets: sets
        };
    }

    mergeShapes(){
        var sortedSets = _.orderBy(this.state.dataSets, ['rate'], ['desc']);

        let dataSets = [
            {
                rate: 0,
                dataSet: _.union(sortedSets[0].dataSet, sortedSets[1].dataSet)
            },
            {
                rate: 1,
                dataSet: _.union(sortedSets[2].dataSet, sortedSets[3].dataSet)
            }
        ];

        this.setState((prevState, props) => {
            return {
                iteration: prevState.iteration + 1,
                sets: 2,
                dataSets: dataSets
            };
        });
    }

    setInitialData(){
        window.CONSTANTS = {
            elemWidth: 10,
            center: {
                x: 200,
                y: 200
            }
        };
    }

    getInitialDataSets(){
        let sets = [];
        for(var i = 0; i<5; i++){
            let transformer = new TransformController();
            var ds = transformer.transformDataset(
                [{
                    "x": window.CONSTANTS.center.x,
                    "y": window.CONSTANTS.center.y
                }]);

            sets.push(
                {
                    rate: ds.rate,
                    dataSet: ds.dataSet
                });
        }
        return sets;
    }

    render() {

        var that = this;
        var grammars = [];
        for(var i =0; i < this.state.sets; i++){
            let myDs = that.state.dataSets[i];
            grammars.push(<div className="col-lg-2" key={i}>
                <Grammar dataSet={myDs.dataSet} rate={myDs.rate} key={i} index={i}></Grammar>
            </div>);
        }
        return (
          <div className="App">
            <div className="row">
                <div className="h1">Iteration {that.state.iteration}</div>
            </div>
              <div className="row">
                  <button className="btn-primary" onClick={that.mergeShapes.bind(that)}>Merge shapes</button>
              </div>
            <div className="row">
                {grammars}
            </div>
          </div>
        );
    }
}

export default App;
